require 'spec_helper'
require './session_three.rb'

RSpec.describe SessionThree, type: :class do 
  describe ".triple_double" do 
    context "when two numbers are passed in" do
      it "returns one if the first number passed in has three of the same numbers consecutively and the second number being passed in has two of the same numbers consecutively" do 
        num = 451999277
        num_two = 41177722899
        expect(SessionThree.triple_double(num, num_two)).to eq 1
      end
    end

    context "when two numbers are passed in" do
      it "returns one if the first number passed in has three of the same numbers consecutively and the second number being passed in has two of the same numbers consecutively" do 
        num = 67844 
        num_two = 5679 
        expect(SessionThree.triple_double(num, num_two)).to eq 0 
      end
    end
  end

  describe "coin_determiner" do 
    context "when a number is passed in" do 
      it "returns the amount of numbers need to equal some of the number being passed in" do 
        num = 250
        expect(SessionThree.coin_determiner(num)).to eq 3
      end
    end
  end

  describe "duplicates" do 
    context "when an array is passed in" do 
      it "returns a hash with the value being the number of occurances" do 
        arr = [100,2,101,4]
        expect(SessionThree.dups(arr)).to eq 0 
      end
    end
  end

  describe "binary_gap" do 
    context "when a decimal number is passed in" do
      it "return the largest binary gap" do
        num  = 99 
        num2 = 97
        num3 = 93
        num4 = 40
        num5 = 16
        num7 = 6 
        num6 = 0
        expect(SessionThree.binary_gap(num)).to eq 3
        expect(SessionThree.binary_gap(num2)).to eq 4
        expect(SessionThree.binary_gap(num3)).to eq 1 
        expect(SessionThree.binary_gap(num4)).to eq 3 
        expect(SessionThree.binary_gap(num4)).to eq 3 
        expect(SessionThree.binary_gap(num5)).to eq 0 
        expect(SessionThree.binary_gap(num6)).to eq 0 
        expect(SessionThree.binary_gap(num7)).to eq 0 
      end
    end
  end
  
  describe "frogImp" do 
    context "when starting pos desired pos and allowed jumps are passed in" do 
      it "returns number of jumps need to reach desired position" do 
        x = 10
        y = 85
        d = 30
        a = 5
        b = 90
        c = 15
        expect(SessionThree.frog_imp(x, y, d)).to eq 3
        expect(SessionThree.frog_imp(a, b, c)).to eq 6
      end
    end
  end

  describe "permMissing" do 
    context "when an array of int are passed in" do 
      it "returns the missing int" do 
        arr = [2, 3, 1, 5]
        arr3 = [1, 2]
        arr1 = [1]
        arr2 = []
        expect(SessionThree.perm_missing(arr)).to eq 4
        expect(SessionThree.perm_missing(arr1)).to eq 2 
        expect(SessionThree.perm_missing(arr2)).to eq 1
        expect(SessionThree.perm_missing(arr3)).to eq 3
      end
    end
  end 
end























