require 'spec_helper'
require './session_two'

RSpec.describe SessionTwo, type: :class do 
  describe "cearser cipher" do 
    context 'when a string and a number is passed in' do 
      it "it increments every letter to the target number passed in" do 
        str = 'Caesar Cipher'
        num = 2
        expect(SessionTwo.caesar_cipher(str, num)).to eq "Ecguct Ekrjgt"
      end
    end
  end
  describe "dupes" do 
    it "" do
      arr = ['a', 'a', 'b', 'b', 'c', 'g']
      expect(SessionTwo.dupes(arr)).to eq {}
    end
  end


  describe ".formated division" do 
    context 'when two numbers are passed into the formatted division method' do 
      it "returns a float with properly placed commas" do
        num1 = 123456789 
        num2 = 10000
        expect(SessionTwo.formatted_division(num1, num2)).to eq "12,345.6789"
      end
    end
  end

  describe ".swap" do 
    context "when a string is passed in and has a substring with more than one digit" do 
      it "swaps the two digits if more than one index exist between them" do
        str = "Hello -5LOL6" 
        expect(SessionTwo.swap(str)).to eq "hELLO -6lol5"
      end
    end
  end

  describe ".alphabet_searching" do
    context "when a string is passed in" do 
      it "returns true if every letter in the alphabet exits in the string" do 
        #str = "sfdsfdfdsfabcdefghsfdssdfdsijklmnopqrstuvwxyz"
        str = "abcdefghijklmnopqrstuvwxyz" 
        expect(SessionTwo.alphabet_searching(str)).to eq true
      end
    end

    context "when a string is passed in" do 
      it "returns true if every letter in the alphabet exits in the string" do 
        str = "ddddddefghijklmngggopqustuvaaa"
        expect(SessionTwo.alphabet_searching(str)).to eq false
      end
    end
  end
  
  describe ".prime_mover" do 
    context "when a number is passed in" do 
      it "returns the numth prime number" do 
        num = 16
        expect(SessionTwo.prime_mover(num)).to eq 53 
      end
    end
  end

  describe ".dash_insert2" do 
    context "when a string of numbers is passed in" do 
      it "returns a string with * between even numbers and a dash between odd numbers" do 
        str2 = '99946'
        str = '56647304'
        expect(SessionTwo.dash_insert2(str)).to eq "56*6*47-304"
        expect(SessionTwo.dash_insert2(str2)).to eq "9-9-94*6"
      end
    end
  end

  describe ".triple_double" do 
    context "when two numbers are passed in" do 
      it "returns 1 if first number passed in has 3 digits in a row, second number has 2 digits in a row" do 
        num = 451999277
        num2 = 41177722899
        expect(SessionTwo.triple_double(num, num2)).to eq 1
      end
    end
  end
  describe ".triple_double" do 
    context "when two numbers are passed in" do 
      it "returns 1 if first number passed in has 3 digits in a row, second number has 2 digits in a row" do 
        num =  67844 
        num2 = 66237
        expect(SessionTwo.triple_double(num, num2)).to eq 0
      end
    end
  end

  describe ".nuber_search" do 
    context "when a string is passed in" do 
      it "add up all the numbers in the string dives that sum by the length on letters in the strig" do 
        str = "H3ello9-9"
        str2 = "Hello6 9World 2, Nic8e D7ay!"
        expect(SessionTwo.number_search(str)).to eq 4
        expect(SessionTwo.number_search(str2)).to eq 2
      end
    end
  end

  describe "most_free_time" do 
    context "when an array of events are passed in" do 
      it "returns the most free time inbetween events" do 
        array =  ["10:00AM-12:30PM","02:00PM-02:45PM","09:10AM-09:50AM"]
        expect(SessionTwo.most_free_time(array))
      end
    end
  end
end












