require  'spec_helper'
require  './session_one'

RSpec.describe SessionOne, type: :class do 

  describe "reverse_string" do 
    context "when a string is passed in" do 
      it "returns sting in reverse" do 
        string = "superduper"
        expect(SessionOne.reverse_string(string)).to eq "repudrepus"
      end
    end
  end

  describe "palidrome" do 
    context "when a string is passed in" do 
      it "will detemine if the string is a palidrome" do 
        string = "Madam"
        expect(SessionOne.palidrome(string)).to eq true
      end
    end
  end

  describe "reverse_int" do 
    context "when an int is passed in" do 
      it "will reverse the int" do 

      end
    end
  end

  describe "capitalize_letters" do 
    context "when a string is passed in" do 
      it "returns the first letter in each word capitalized" do 

      end
    end
  end
  
  #when I pass a string into max_character method, it returns the number of times each letter appears in a string 
  #input string of letters 
  #expected output hash with a value that represents that letters number of occurances in the given string. 
  describe "max_character" do 
    context "when an string is passed in" do 
      it "returns the character most common in a string" do 
        str = "abcaaczunn"
        expect(SessionOne.max_character(str)).to eq '' 
      end
    end
  end

  describe "fizz_buzz" do 
    it "will return fizz for mul of 3 buzz for mul of 5 fizzbuzz for mul of 3&5" do 
      expect(SessionOne.fizz_buzz).to eq ''
    end
  end

  describe "logest_word" do 
    context "when a string is passed in" do 
      it "returns the logest word in a given string" do 
        array = ['test', 'philadelpia', 'Columbia', 'error']
        expect(SessionOne.longest_word(array)).to "philadelpia"
      end
    end
  end

  describe "chunk_array" do 
    context "when an array is passed in" do 
      it "splits the array into a specific length" do 

      end
    end
  end

  describe "flatten_array" do 
    context "when an array of arrays is passed in" do 
      it "returns a flattened array " do 
    
      end
    end
  end

  describe "is_anagram" do 
    context "when a string is passed in" do 
      it "returns a string of letters that follow letters passed in and capitalizes the vowels" do
        string = "hello"
        expect(SessionOne.isAnagram(string)).to eq str = "Ifmmp"
      end
    end
  end

  describe "logest sub string" do
    context "when a string is passed in" do 
      it "returns the logest substring" do
        str = "abcabcbb"
        expect(SessionOne.longest_substring(str)).to eq "abc"
      end
    end
  end

  describe "Age" do 
    context 'when a hash is passed in' do 
      it 'returns number of items with a value equal to or greater than 50' do 
        h = {'a' => 25, 'b' => 50, 'c' => 55, 'e' => 60, 'f' => 30, 'g' => 'qwr'}
        expect(SessionOne.age(h)).to eq 3 
      end
    end
  end

  describe 'when two list containing seperate intergers are passed in' do 
    it 'reverses the intergers, joins them and returns the sum' do 
      list_one = [3, 4, 8]  
      list_two = [5, 7, 9]
      expect(SessionOne.add_two_numbers(list_one, list_two)).to eq 1818
    end
   end

  describe "PalindromicSubstring" do 
    context "when a string is passed in" do 
      it "returns a substring which is a palidrone" do 
         str = "hellosannasmith" 
         expect(SessionOne.palindromic_substring(str))
      end
    end
  end
 
  describe "Ruby age counting" do
    context "when a request is made" do 
      it "returns the number of items that exist with an age equal to or greater than 50" do
      h = Hash.new(0)
      hash =  {"data":"key=IAfpK, age=58, key=WNVdi, age=64, key=jp9zt, age=47, key=0Sr4C, age=68, key=CGEqo, age=76, key=IxKVQ, age=79, key=eD221, age=29, key=XZbHV, age=32, key=k1SN5, age=88, key=4SCsU, age=65" }
      h = hash
      expect(SessionOne.ruby_age_counting(h)).to eq ""
      end
    end
  end
  describe "prime checker" do 
    context "when a number is passed in" do 
      it "determines whether or not the number an be rearanged into a prime number" do 
        num = 910 
        expect(SessionOne.prime_checker(num)).to eq 1 
      end
    end
  end
end




