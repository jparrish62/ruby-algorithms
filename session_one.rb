require 'pry'

class SessionOne

  def self.reverse_string(string)
    arr = string.split('')
    str = ""
    index = arr.length - 1
    stop = 0
    while index > stop do   
      arr.each do |i|
       str = str + arr[index] 
       index = index - 1
      end
    end
    str 
  end

  def self.palidrome(str)
    i = 0
    arr = str.downcase.split('')
    palidrome = false
    while  arr.length > 1 do 
      first = arr.shift()
      last  = arr.pop()
      if first == last 
        palidrome = true
        i = i + 1
      else 
        palidrome = false 
      end
    end
    palidrome
  end
  
  def self.find_palindromic_substring(hash)
    arr = []
    hash.each do |key, value|
        arr << key
    end
    binding.pry
    arr
  end

  def self.new_hash(array, str)
    ch = 1
    index = 1 
    length = str.length
    new_hash = Hash.new(0)
    while index < length do 
      array.each do |i|
        arr1 = i.scan(/.{#{ch}}/)
        arr1.each do |key, _|
          new_hash[key] += 1
        end
      end
      index = index + 1
      ch = ch + 1
    end
    SessionOne.find_palindromic_substring(new_hash)
  end

  def self.new_array(hash, str)
    new_array = []
    hash.each_with_index do |(_, value), index|
      if value > 1
        new_array << str[index..-1]
      end
    end
    SessionOne.new_hash(new_array, str)
  end


  def self.palindromic_substring(str)
    arr  = str.split('')
    hash = Hash.new(0)
    arr.each do |i|
      hash[i] += 1
    end
    SessionOne.new_array(hash, str)
  end


  def self.prime_checker(num)
    n = num.to_s.length
    str = num.to_s.split('')
    arr = str.permutation(n).to_a
    new_array = []
    arr.each do |i|
      inter = i.join().to_i 
      if inter % 2 != 0
        new_array << inter
      end
    end
    if new_array.empty?
      0
    else
      1
    end
  end

  def self.ruby_age_counting(data)
    arr = []
    new_array = []
    data.each do |key, value|
      arr = value.split(',')
    end
    arr.each do |i|
       item = i.scan(/\d/).join.to_i 
       if item >= 50
        new_array << item
       end
    end
    new_array.count
  end

  def self.add_two_numbers(list_one, list_two)
    index = 0
    new_list_one = []
    new_list_two = []
    length = list_one.length
    while index < length do 
       new_list_one << list_one.pop
       new_list_two << list_two.pop
       index = index + 1
    end
    new_list_one.join('').to_i + new_list_two.join('').to_i
  end

  #def self.reverse_int(int)
  #end

  #def self.capitalize_letters(str)
  #end

  #when I pass a string into max_character method, it returns the number of times each letter appears in a string 
  #input string of letters 
  #
  #
  
  def self.longest_substring(str) 
    index = 0
    ch = 1
    length = str.length 
    hash = Hash.new(0)
    new_array = []
    while index < length do 
      arr = str.scan(/.{#{ch}}/)
      arr.each do |key, _|
        hash[key] += 1
      end
      ch = ch + 1
      index = index + 1
    end
    hash.each do |key, value|
      if (key.length > 1) && (value > 1)
        new_array << key
      end
    end
    new_array[0].length
  end

  def self.max_character(str)
    arr = str.split('')
    new_array = []
    my_array = []
    h = Hash.new(0)
    arr.each do |key|
      h[key] += 1
    end
    h.each do |key, value|
      new_array << value 
      if h[key] == new_array.max()
        my_array << key
      end
    end
    my_array.max()
  end

  def self.fizz_buzz
    1.upto(100)  do |i|
      if i % 3 == 0
        puts fizz
      elsif i % 5 == 0
        puts buzz
      elsif i % 3 && i % 5 == 0
        puts fizzbuzz
      else 
        puts i
      end
    end
  end

  def self.longest_word(array)
    word = ""
    array.each do |i|
      next_word = array[i + 1]
      if array[i] > next_word 
        word = next_word
      else
        word = array[i]
      end
    end
    word
  end

  #def self.chunk_array(array)
  #end

  #def self.flatten_array(array)
  #end

  def capitalize_vowels(array)
    new_array = []
    array.each do |i|
      if /aeiou/.match(array[i])
        new_array.push(array[i].capitalize)
      else
        new_array.push(array[i])
      end
    end
    new_array.join('')
  end

  def self.isAnagram(string, index=0, i=0)
    str = string.downcase
    letters = 'abcdefghijklmnopqrstuvwxyz'   
    array = []
    arr = str.split(', ')
    while i < str.length do 
      if arr[i] == letters[index]
        array.push(letters[index + 1])
        i = i + 1
        SessionOne.isAnagram(string, i)
      else
        index = index + 1
        SessionOne.isAnagram(string, index)
      end
    end
    SessionOne.capitalize_vowels(array)
  end

  def self.age(hash)
    arr = []
    hash.each do |key, _|
      if hash[key].to_i >= 50
        arr << key
      end
    end
    arr.length
  end
end

















