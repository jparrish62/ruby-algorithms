require 'pry'
require 'time'
require 'Prime'

class SessionTwo
  def self.dupes(array)
    b = array.join
    h = {}
    array.each do |i|
      h[i] = b.scan.length
    end
     
  end

  def self.caesar_cipher(str, num)
    char = 'abcdefghijklmnopqrstuvwxyz'.split('')
    arr = []
    index = 0 
    str_arr = str.downcase.split('')
    length = str_arr.length
    while index < length do 
      char.each_with_index do |v, i|  
        c = str_arr[index]
        if v == c 
          arr << char[i + num]
          index = index + 1
        elsif c.to_s == " "
          arr << " " 
          index = index + 1
        end
      end
    end
    arr.join().split(' ').map(&:capitalize).join(' ')
  end

  def self.formatted_division(num1, num2)
    result = num1/num2.to_f
    arr = result.to_s.split('.')
    after_decimal = arr.pop() 
    new_array = arr.join.reverse.split('')
    resulting_array = []
    new_array.each do |i|
      if (resulting_array.length > 1) && (resulting_array.length % 3 == 0)
        resulting_array << "," + i
      else 
        resulting_array << i
      end
    end
    resulting_array.join.reverse + "." + after_decimal 
  end

 def self.swap(str) 
   arr = str.split(' ')
   arr.each_with_index do |i, ind|
     digits_array = i.scan(/\d/)
     string_array = i.split('')
     if digits_array.length > 1
       a = string_array.index(digits_array[0])
       b = string_array.index(digits_array[1])
       string_array[a], string_array[b] = string_array[b], string_array[a] 
       arr[ind] = string_array.join
     end
   end
   arr.each_with_index do |i, n_index|
     string_arr = i.split('')
     string_arr.each_with_index do |item, s_index|
       if item.scan(/\d/).length == 0
         string_arr[s_index] = string_arr[s_index].swapcase
         arr[n_index] = string_arr.join
       end
     end
   end
   arr.join(' ')
 end

 def self.alphabet_searching(str)
   alphabet = "abcdefghijklmnopqrstuvwxyz" 
   alphabet_array = alphabet.split('')
   alphabet_characters = true
   alphabet_array.each do |i|
     if str.scan(/#{i}/).empty? == true
       return alphabet_characters = false
     end
   end
   alphabet_characters 
 end

 def self.prime_mover(num)
   Prime.first(num).last
 end

 def self.dash_insert2(str)
   string_array = str.split('')
   new_array = []
   string_array.each_with_index do |n, i|
     if string_array[i].to_i !=0 && (string_array[i].to_i % 2) == 0 && (string_array[i + 1].to_i % 2) == 0 
        new_array << n + "" + "*" 
     elsif string_array[i.to_i] &&(string_array[i].to_i % 2) != 0 && (string_array[i + 1].to_i % 2) != 0 
        new_array << n + "" + "-"
     else
       new_array << n 
     end
   end
   arr = new_array.join.split('')
   arr.pop
   arr.join 
 end

 def self.triple_double(num, num2)
   out_put = 0
   array_one = num.to_s.split('')
   array_two = num2.to_s.split('')
   array_one.each_with_index do |_, i|
     if array_one[i].to_i == array_one[i + 1].to_i && array_one[i].to_i == array_one[i + 2].to_i
       array_two.each_with_index do |_, i|
         if array_two[i].to_i == array_two[i + 1].to_i
           out_put = 1
         end
       end
     end
   end
   out_put 
 end

 def self.number_search(str)
   num_array = str.scan(/\d/) 
   letter_array = str.scan(/[a-zA-Z]/)
   sum = 0
   num_array.each do |i| 
     sum = sum + i.to_i 
   end
   quotient = sum.to_f / letter_array.length.to_f
   quotient.round 
 end

 def self.most_free_time(array)
   new_array = []
   array.each do |v, i|
     t_array = v.split("-")
     if  (t_array[0].include? "PM") && (t_array[1].include? "PM")
       a = t_array[0].scan(/\d/).join.to_i + 1200 
       b = t_array[1].scan(/\d/).join.to_i + 1200 
       new_array << a.to_s + "-" + b.to_s
     elsif (t_array[0].include? "AM") && (t_array[1].include? "PM")
       a = t_array[0].scan(/\d/).join.to_i 
       b = t_array[1].scan(/\d/).join.to_i + 1200 
       new_array << a.to_s + "-" + b.to_s
     elsif (t_array[0].include? "AM") && (t_array[1].include? "AM")
       a = t_array[0].scan(/\d/).join.to_i 
       b = t_array[1].scan(/\d/).join.to_i  
       new_array << a.to_s + "-" + b.to_s
     end
   end
   new_array
 end
end














