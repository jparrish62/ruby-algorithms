require 'pry'

class SessionThree 

  def self.triple_double(num, num_two) 
   array_num_one = num.to_s.split('')
   array_num_two = num_two.to_s.split('') 
   result = 0
   index = 0
   next_index = index + 1 
   while index <= array_num_one.length-1 do 
     if array_num_one[index] == array_num_one[next_index] && array_num_one[index] == array_num_one[next_index + 1]
       index = 0
       next_index = index + 1
       while index <= array_num_two.length do 
         if array_num_two[index] == array_num_two[next_index]
           result = 1
           return result
         else
           index = index + 1
           next_index = index + 1 
         end
       end
     else
       index = index + 1
       next_index = index + 1 
     end
   end
   result
  end

  def self.coin_determiner(num)
    return num if num <= 4 && num >= 0
    count, coins = 0, [11, 9, 7, 5, 3, 1]
    coins.each do |coin|
      while num >= coin
        count += 1 and num -= coin
      end
    end
  count
  end

  def self.dups(arr)
    hash = Hash.new(0)
    new_array = []
    arr.each { |e| hash[e] += 1 }
    hash.each  { |k, v| new_array << v - 1 if v > 1 }
    new_array.sum
  end

  def self.binary_gap(num)
    array = [num]
    new_array = []
    while num > 1 
      array << num / 2
      num = num / 2
    end
    array.each do |i|
      new_array << i % 2
    end
    binary_rep = new_array.reverse.join
    return 0 if binary_rep.scan(/10+1/).empty?
    binary_rep.split('1').max(1).join.length
  end
  
  def self.frog_imp(x, y, d)
    a = y - x 
    b = a.to_f / d.to_f
    b.to_f.ceil
  end
  
  def self.perm_missing(arr)
    return 1 if arr.empty?
    array = arr.sort
    reference_array = 1.upto(array[-1]).to_a
    results = reference_array - array
    if results.empty?
      return array[-1] + 1
    else
     return results.pop
    end
  end
end











